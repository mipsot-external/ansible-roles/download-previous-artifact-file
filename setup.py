from setuptools import setup, find_packages

setup(
    name='mipsot.download-previous-artifact-file',
    version='0.1',
    description='Downloader for previous successfully finished Gitlab pipeline artifacts',
    url='https://gitlab.com/mipsot-external/ansible-roles/download-previous-artifact-file',
    author='https://gitlab.com/mipsot',
    author_email='mipsot@ya.ru',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
    ],
    keywords='git gitlab runner gitlab-runner artifact',
    package_dir={'': '.'},
    packages=find_packages(where='.'),
    package_data={
        'ansible_role': [
            'meta',
            'vars',
            'handlers',
            'tasks',
        ],
    },
    project_urls={
        'Bug Reports': 'https://gitlab.com/mipsot-external/ansible-roles/download-previous-artifact-file/-/issues',
        'Source': 'https://gitlab.com/mipsot-external/ansible-roles/download-previous-artifact-file',
    },
)