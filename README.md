Role Name
=========

Downloader for previous successfully finished Gitlab pipeline artifacts. Only single file yet allowed. Should be used in runners.

Installing
----------

```
ansible-galaxy install git+https://gitlab.com/mipsot-external/ansible-roles/download-previous-artifact-file.git
```

Role Variables
--------------

* `GITLAB_ACCESS_TOKEN_API` &mdash; access_token with `api` scope to download artifacts.
* `ARTIFACT_FILE_PATH` &mdash; file path which should be downloaded.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
      - role: download-previous-artifact-file
        vars:
          GITLAB_ACCESS_TOKEN_API: "{{ lookup('file', GITLAB_ACCESS_TOKEN_API_FILE) }}"
          ARTIFACT_FILE_PATH: artifacts/target_file

Author Information
------------------

https://gitlab.com/mipsot